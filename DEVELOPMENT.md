Руководство по разработке компонента

Процесс:

[[_TOC_]]

## Подготовка

Создайте ветку с названием компонента.

## <span id="1-исследование">1. Исследование</span>

Изучите работу компонента. Как он должен работать? Какие задачи решать? Посмотрите существующий компонент в личном кабинете: сравните подход, подумайте как можно улучшить. 

Если есть вопросы, посоветуйтесь с дизайнером или ведущим разработчиком.

Примеры дизайн-систем:

[Контур](https://guides.kontur.ru/)  
[Альфа-банк](https://alfa-laboratory.github.io/arui-feather/styleguide)  
[Ant Design](https://ant.design/)  
[Material UI](https://mui.com/)  
[NordDesign](https://nordhealth.design/)  
[и другие](https://ru.react.js.org/community/ui-components.html)

## <span id="2-документация">2. Документация</span>

Опишите функционал и состояния компонента.

Создайте истории (документированные состояния и возможности компонента) в [Сторибуке](https://storybook.js.org/). Пишите в формате [mdx](https://storybook.js.org/docs/react/writing-docs/mdx). 

Документация может состоять из отдельных историй, отражающих различные состояния компонента. Каждую отдельную историю пишите в формате [csf](https://storybook.js.org/docs/react/api/csf).

Обязательно наличие истории с описанием props и истории с возможностью интерактивного просмотра компонента (controls).

Официальная документация:  
[Doc Blocks](https://storybook.js.org/docs/react/writing-docs/doc-blocks#argstable) — построение таблицы props.  
[Controls](https://storybook.js.org/docs/react/essentials/controls) — добавление интерактивных элементов для взаимодействия с историей.

## <span id="3-тестирование">3. Тестирование</span>

Напишите минимальный набор тестов (TDD).

По теме:  
[Что и как тестировать с помощью Jest и Enzyme. Полная инструкция по тестированию React-компонентов](https://medium.com/devschacht/what-and-how-to-test-with-jest-and-enzyme-full-instruction-on-react-components-testing-d3504f3fbc54)  
[Enzyme](https://enzymejs.github.io/enzyme/)  
[Enzyme API](https://enzymejs.github.io/enzyme/docs/api/)  
[Enzyme cheatsheet](https://devhints.io/enzyme)

## <span id="4-функционал">4. Функционал</span>

Напишите функционал компонента.

Старайтесь использовать нативные свойства и методы простого DOM-элемента. Обеспечьте всплытие и проброс переданных в компоненте свойств. 

Не забывайте о доступности (accessibility, aria-attributes), управлении с клавиатуры, touch-устройствах, работе с фокусом.

## <span id="5-дизайн">5. Дизайн</span>

Стилизуйте компонент: вынесите основные стили и свойства в тему компонента. Доработайте Сторибук для возможности интерактивной настройки темы.

Некоторые браузеры могут не поддерживать используемые CSS-правила. Проверьте их в сервисе [CAN I USE](https://caniuse.com/).

[Компоненты в Фигме](https://www.figma.com/file/IdrgT5gbVCz4Xg9Zcv2ZSz). 
Если нужен доступ, напишите [дизайнеру](mailto:eugeny.starikov@payanyway.ru).

## <span id="6-интеграция">6. Интеграция</span>

Интегрируйте компонент в PAW backoffice:

1. Создайте отдельную ветку в проекте paw.
2. Создать мёрдж-реквест в проекте библиотеки компонентов.
