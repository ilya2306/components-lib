import React from 'react';
import { ThemeProvider } from 'styled-components';

import { createTheme } from '../src/theme/utils/create-theme';


export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  viewMode: 'docs',
  options: {
    storySort: {
      order: ['Introduction', 'Components', ['Controls']],
    },
  },
};

const theme = createTheme({});

export const decorators = [
  (Story) => (
    <ThemeProvider theme={theme}>
      <Story />
    </ThemeProvider>
  ),
];
