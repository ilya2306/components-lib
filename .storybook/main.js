const path = require('path');

module.exports = {
  stories: ['../src/**/*.stories.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: [
    '@storybook/addon-docs',
    '@storybook/addon-links',
    '@storybook/addon-controls',
    '@storybook/addon-a11y',
    '@storybook/addon-essentials',
    '@storybook/addon-actions',
  ],
  webpackFinal: async (config, { configType }) => {
    config.resolve = {
      ...config.resolve,
      modules: [
        ...config.resolve.modules || [],
        path.resolve(__dirname, '../src'),
      ],
    };


    return config;
  },
};
