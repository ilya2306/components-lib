import { addons } from '@storybook/addons';

import theme from "./theme";

addons.setConfig({
  theme: theme,
  isFullscreen: false,
  showNav: true,
  showPanel: true,
  panelPosition: 'bottom',
  sidebarAnimations: false,
  enableShortcuts: false,
  isToolshown: true,
  sidebar: {
    showRoots: false,
  },
});
