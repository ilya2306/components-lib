import { Amber } from './Amber';
import { Blue } from './Blue';
import { BlueGrey } from './BlueGrey';
import { Brown } from './Brown';
import { Cyan } from './Cyan';
import { DeepOrange } from './DeepOrange';
import { DeepPurple } from './DeepPurple';
import { Green } from './Green';
import { Grey } from './Grey';
import { Indigo } from './Indigo';
import { LightBlue } from './LightBlue';
import { LightGreen } from './LightGreen';
import { Lime } from './Lime';
import { Orange } from './Orange';
import { Pink } from './Pink';
import { Purple } from './Purple';
import { Red } from './Red';
import { Teal } from './Teal';
import { Yellow } from './Yellow';

import { hexToRGBA } from './utils';

export const Black = '#000000';
export const White = '#FFFFFF';

export default {
  Amber,
  Black,
  Blue,
  BlueGrey,
  Brown,
  Cyan,
  DeepOrange,
  DeepPurple,
  Green,
  Grey,
  Indigo,
  LightBlue,
  LightGreen,
  Lime,
  Orange,
  Pink,
  Purple,
  Red,
  Teal,
  White,
  Yellow,
  hexToRGBA,
};
