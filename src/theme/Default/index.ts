import { ThemeType } from '../@typings/theme';

import { Button } from '../../components/Button/theme';

export const Default: ThemeType = {
  Button,
};
