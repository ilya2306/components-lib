import { ThemeType } from '../@typings/theme';

import { deepMerge } from './utils';

import { Default } from 'theme/Default';
// import { ButtonVariantType } from '../../components/Button/@typings';
// import { background } from '@storybook/theming';


export const createTheme = (theme: ThemeType = {}) => {
// Component.common.static
// Component.common[state]
// Component.variants[variant].static
// Component.variants[variant][state]
//
//
//
//   {
//     common: {
//       static: {
//         background: 'csb'
//       }
//     },
//     variants: {
//       secondary: {
//         static: {
//           background: "vssb"
//         }
//       },
//       danger: {
//         disabled: {
//           background: ""
//         }
//       }
//     }
//   }
//
// primary.disabled.background // ====  csb
// secondary.hover.background // === vssb
// link.disabled.color // === defaultLink
//
//
// const getPropertyValue(theme: ThemeType, varaint: ButtonVariantType, state: keyof ButtonVariantType, name: string) => {
//   if(theme has variant)
//     if (theme.variant has state)
//       if (theme.variant.state has name)
//         theme.variant.state.name
//     else if(theme.variant.static.name)
//       theme.variant.static.name
//   else if theme.common.state.name
//       theme.common.state.name
//   else theme.common.static.name
//     return defaultTheme value;
//   }

  return deepMerge(Default, theme);
};
