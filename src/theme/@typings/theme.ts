import { ButtonThemeType } from '../../components/Button/theme/@typings';

export type ThemeType = {
  Button?: ButtonThemeType;
};
