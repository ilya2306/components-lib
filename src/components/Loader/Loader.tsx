import React from 'react';

import { LoaderProps } from './@typings';

import { StyledLoader } from './style/StyledLoader';
import { StyledCircle } from './style/StyledCircle';

export const Loader: React.FC<LoaderProps> = (props) => (
  <StyledLoader {...props}>
    <StyledCircle />
    <StyledCircle />
    <StyledCircle />
  </StyledLoader>
);

Loader.defaultProps = {
  duration: '1s',
};
