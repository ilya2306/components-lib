import styled from 'styled-components';

import { LoaderProps } from '../@typings';

export const StyledLoader = styled.div<LoaderProps>`
  display: flex;
  align-items: center;
  justify-content: space-between;
  animation-duration: ${(props) => props.duration};
  min-width: 40px;

  & > div:nth-child(1) {
    background-color: ${(props) => (props.monochrome ? 'currentColor' : '#FBC753')};
  }

  & > div:nth-child(2) {
    background-color: ${(props) => (props.monochrome ? 'currentColor' : '#92C74C')};
    animation-delay: 0.15s;
  }

  & > div:nth-child(3) {
    background-color: ${(props) => (props.monochrome ? 'currentColor' : '#EE5A85')};
    animation-delay: 0.3s;
  }
`;
