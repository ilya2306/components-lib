import styled, { keyframes } from 'styled-components';

type CircleProps = {};

const pulse = keyframes`
  0% {
    transform: scale(1);
  }

  50% {
    transform: scale(0.1);
  }

  100% {
    transform: scale(1);
  }
`;

export const StyledCircle = styled.div<CircleProps>`
  width: 8px;
  height: 8px;
  border-radius: 50%;
  animation-play-state: running;
  animation-name: ${pulse};
  animation-duration: inherit;
  animation-timing-function: linear;
  animation-iteration-count: infinite;
  animation-direction: normal;
  animation-fill-mode: none;
`;
