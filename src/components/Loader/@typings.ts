import { CSSAnimationDurationType } from '../../theme/@typings/common';

export type LoaderProps = {
  duration?: CSSAnimationDurationType;
  monochrome?: boolean;
};
