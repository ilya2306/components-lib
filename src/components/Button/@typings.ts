import React from 'react';

export const ButtonSize = {
  Large: 'large',
  Medium: 'medium',
  Small: 'small',
} as const;

export type ButtonSizeType = typeof ButtonSize[keyof typeof ButtonSize];

export const ButtonVariant = {
  Danger: 'danger',
  Link: 'link',
  Primary: 'primary',
  Secondary: 'secondary',
} as const;

export type ButtonVariantType = typeof ButtonVariant[keyof typeof ButtonVariant];

export const ButtonIconPosition = {
  Left: 'left',
  Right: 'right',
} as const;

export type ButtonIconPositionType = typeof ButtonIconPosition[keyof typeof ButtonIconPosition];

export interface ButtonProps
  extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
  /**
   * Ссылка
   */
  href?: string;
  /**
   * Элемент иконки
   */
  icon?: React.ReactNode;
  /**
   * Позиция иконки
   */
  iconPosition?: ButtonIconPositionType;
  /**
   * Состояние загрузки
   */
  loading?: boolean;
  /**
   * Размер
   */
  size?: ButtonSizeType;
  /**
   * Стиль
   */
  variant?: ButtonVariantType;
}
