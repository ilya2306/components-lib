import { Button as Theme } from '../theme';
import { ButtonIconPosition, ButtonSize, ButtonVariant, ButtonVariantType } from '../@typings';
import { ButtonVariantsRules } from '../theme/@typings';
import { Button } from '../Button';

function firstLetterUppercase(value: string) {
  return value.replace(/^(\S)/, (_all, f) => f.toUpperCase());
}

function getBackground(variant: ButtonVariantType, state: keyof ButtonVariantsRules, description?: string) {
  const defaultValue = Theme.variants?.[variant]?.[state]?.background;

  return {
    name: 'background',
    description: description || `Background of ${variant} variant in ${state} state`,
    control: {
      type: 'text',
    },
    defaultValue,
    table: {
      category: `Variant.${firstLetterUppercase(variant)}`,
      subcategory: firstLetterUppercase(state),
      type: {
        summary: 'background',
      },
      defaultValue: { summary: defaultValue },
    },
  };
}

function getBorderColor(variant: ButtonVariantType, state: keyof ButtonVariantsRules, description?: string) {
  const defaultValue = Theme.variants?.[variant]?.[state]?.borderColor;

  return {
    name: 'borderColor',
    description: description || `Border color of ${variant} variant in ${state} state`,
    control: {
      type: 'color',
    },
    defaultValue,
    table: {
      category: `Variant.${firstLetterUppercase(variant)}`,
      subcategory: firstLetterUppercase(state),
      type: {
        summary: 'borderColor',
      },
      defaultValue: { summary: defaultValue },
    },
  };
}

function getBorderStyle(variant: ButtonVariantType, state: keyof ButtonVariantsRules, description?: string) {
  const defaultValue = Theme.variants?.[variant]?.[state]?.borderStyle;

  return {
    name: 'borderStyle',
    description: description || `Border style of ${variant} variant in ${state} state`,
    control: {
      type: 'text',
    },
    defaultValue,
    table: {
      category: `Variant.${firstLetterUppercase(variant)}`,
      subcategory: firstLetterUppercase(state),
      type: {
        summary: 'borderStyle',
      },
      defaultValue: { summary: defaultValue },
    },
  };
}

function getBoxShadow(variant: ButtonVariantType, state: keyof ButtonVariantsRules, description?: string) {
  const defaultValue = Theme.variants?.[variant]?.[state]?.boxShadow;

  return {
    name: 'boxShadow',
    description: description || `Box shadow of ${variant} variant in ${state} state`,
    control: {
      type: 'text',
    },
    defaultValue,
    table: {
      category: `Variant.${firstLetterUppercase(variant)}`,
      subcategory: firstLetterUppercase(state),
      type: {
        summary: 'boxShadow',
      },
      defaultValue: { summary: defaultValue },
    },
  };
}

function getColor(variant: ButtonVariantType, state: keyof ButtonVariantsRules, description?: string) {
  const defaultValue = Theme.variants?.[variant]?.[state]?.color;

  return {
    name: 'color',
    description: description || `Color of ${variant} variant in ${state} state`,
    control: {
      type: 'color',
    },
    defaultValue,
    table: {
      category: `Variant.${firstLetterUppercase(variant)}`,
      subcategory: firstLetterUppercase(state),
      type: {
        summary: 'color',
      },
      defaultValue: { summary: defaultValue },
    },
  };
}

function getOpacity(variant: ButtonVariantType, state: keyof ButtonVariantsRules, description?: string) {
  const defaultValue = Theme.variants?.[variant]?.[state]?.opacity;

  return {
    name: 'opacity',
    description: description || `Opacity of ${variant} variant in ${state} state`,
    control: {
      type: 'number',
      max: 1,
      min: 0,
      step: 0.05,
    },
    defaultValue,
    table: {
      category: `Variant.${firstLetterUppercase(variant)}`,
      subcategory: firstLetterUppercase(state),
      type: {
        summary: 'number',
      },
      defaultValue: { summary: defaultValue },
    },
  };
}

function getOutline(variant: ButtonVariantType, state: keyof ButtonVariantsRules, description?: string) {
  const defaultValue = Theme.variants?.[variant]?.[state]?.outline;

  return {
    name: 'outline',
    description: description || `Outline of ${variant} variant in ${state} state`,
    control: {
      type: 'text',
    },
    defaultValue,
    table: {
      category: `Variant.${firstLetterUppercase(variant)}`,
      subcategory: firstLetterUppercase(state),
      type: {
        summary: 'outline',
      },
      defaultValue: { summary: defaultValue },
    },
  };
}

const getStatesStyles = (variant: ButtonVariantType) => ({
  [`variant${firstLetterUppercase(variant)}StaticBackground`]: getBackground(variant, 'static'),
  [`variant${firstLetterUppercase(variant)}StaticBorderColor`]: getBorderColor(variant, 'static'),
  [`variant${firstLetterUppercase(variant)}StaticBorderStyle`]: getBorderStyle(variant, 'static'),
  [`variant${firstLetterUppercase(variant)}StaticBoxShadow`]: getBoxShadow(variant, 'static'),
  [`variant${firstLetterUppercase(variant)}StaticColor`]: getColor(variant, 'static'),
  [`variant${firstLetterUppercase(variant)}StaticOpacity`]: getOpacity(variant, 'static'),
  [`variant${firstLetterUppercase(variant)}StaticOutline`]: getOutline(variant, 'static'),

  [`variant${firstLetterUppercase(variant)}HoverBackground`]: getBackground(variant, 'hover'),
  [`variant${firstLetterUppercase(variant)}HoverBorderColor`]: getBorderColor(variant, 'hover'),
  [`variant${firstLetterUppercase(variant)}HoverBorderStyle`]: getBorderStyle(variant, 'hover'),
  [`variant${firstLetterUppercase(variant)}HoverBoxShadow`]: getBoxShadow(variant, 'hover'),
  [`variant${firstLetterUppercase(variant)}HoverColor`]: getColor(variant, 'hover'),
  [`variant${firstLetterUppercase(variant)}HoverOpacity`]: getOpacity(variant, 'hover'),
  [`variant${firstLetterUppercase(variant)}HoverOutline`]: getOutline(variant, 'hover'),

  [`variant${firstLetterUppercase(variant)}ActiveBackground`]: getBackground(variant, 'active'),
  [`variant${firstLetterUppercase(variant)}ActiveBorderColor`]: getBorderColor(variant, 'active'),
  [`variant${firstLetterUppercase(variant)}ActiveBorderStyle`]: getBorderStyle(variant, 'active'),
  [`variant${firstLetterUppercase(variant)}ActiveBoxShadow`]: getBoxShadow(variant, 'active'),
  [`variant${firstLetterUppercase(variant)}ActiveColor`]: getColor(variant, 'active'),
  [`variant${firstLetterUppercase(variant)}ActiveOpacity`]: getOpacity(variant, 'active'),
  [`variant${firstLetterUppercase(variant)}ActiveOutline`]: getOutline(variant, 'active'),

  [`variant${firstLetterUppercase(variant)}FocusBackground`]: getBackground(variant, 'focus'),
  [`variant${firstLetterUppercase(variant)}FocusBorderColor`]: getBorderColor(variant, 'focus'),
  [`variant${firstLetterUppercase(variant)}FocusBorderStyle`]: getBorderStyle(variant, 'focus'),
  [`variant${firstLetterUppercase(variant)}FocusBoxShadow`]: getBoxShadow(variant, 'focus'),
  [`variant${firstLetterUppercase(variant)}FocusColor`]: getColor(variant, 'focus'),
  [`variant${firstLetterUppercase(variant)}FocusOpacity`]: getOpacity(variant, 'focus'),
  [`variant${firstLetterUppercase(variant)}FocusOutline`]: getOutline(variant, 'focus'),

  [`variant${firstLetterUppercase(variant)}FocusVisibleBackground`]: getBackground(variant, 'focusVisible'),
  [`variant${firstLetterUppercase(variant)}FocusVisibleBorderColor`]: getBorderColor(variant, 'focusVisible'),
  [`variant${firstLetterUppercase(variant)}FocusVisibleBorderStyle`]: getBorderStyle(variant, 'focusVisible'),
  [`variant${firstLetterUppercase(variant)}FocusVisibleBoxShadow`]: getBoxShadow(variant, 'focusVisible'),
  [`variant${firstLetterUppercase(variant)}FocusVisibleColor`]: getColor(variant, 'focusVisible'),
  [`variant${firstLetterUppercase(variant)}FocusVisibleOpacity`]: getOpacity(variant, 'focusVisible'),
  [`variant${firstLetterUppercase(variant)}FocusVisibleOutline`]: getOutline(variant, 'focusVisible'),

  [`variant${firstLetterUppercase(variant)}DisabledBackground`]: getBackground(variant, 'disabled'),
  [`variant${firstLetterUppercase(variant)}DisabledBorderColor`]: getBorderColor(variant, 'disabled'),
  [`variant${firstLetterUppercase(variant)}DisabledBorderStyle`]: getBorderStyle(variant, 'disabled'),
  [`variant${firstLetterUppercase(variant)}DisabledBoxShadow`]: getBoxShadow(variant, 'disabled'),
  [`variant${firstLetterUppercase(variant)}DisabledColor`]: getColor(variant, 'disabled'),
  [`variant${firstLetterUppercase(variant)}DisabledOpacity`]: getOpacity(variant, 'disabled'),
  [`variant${firstLetterUppercase(variant)}DisabledOutline`]: getOutline(variant, 'disabled'),
});

const SecondaryStatesStyles = getStatesStyles('secondary');
const PrimaryStatesStyles = getStatesStyles('primary');
const DangerStatesStyles = getStatesStyles('danger');
const LinkStatesStyles = getStatesStyles('link');

export const ButtonThemingArgTypes = {
  children: {
    name: 'children',
    table: {
      category: 'Story',
      defaultValue: { summary: 'Action' },
      type: { summary: 'React.ReactChildren' },
    },
    control: { type: 'text' },
    defaultValue: 'Action',
  },
  disabled: {
    name: 'disabled',
    description: 'Компонент не активен',
    table: {
      category: 'Story',
      defaultValue: { summary: 'false' },
      type: { summary: 'boolean' },
    },
    control: { type: 'boolean' },
    defaultValue: false,
  },
  icon: {
    name: 'icon',
    description: 'Иконка',
    table: {
      category: 'Story',
      defaultValue: { summary: undefined },
      type: { summary: 'React.ReactNode' },
    },
    control: { type: 'select', required: false },
    options: ['none', 'send', 'delete'],
    defaultValue: undefined,
  },
  iconPosition: {
    name: 'iconPosition',
    description: 'Расположение иконки',
    table: {
      category: 'Story',
      defaultValue: { summary: Button.defaultProps?.iconPosition },
      type: { summary: `${ButtonIconPosition.Right} | ${ButtonIconPosition.Left}` },
    },
    control: { type: 'select' },
    options: [ButtonIconPosition.Right, ButtonIconPosition.Left],
    defaultValue: Button.defaultProps?.iconPosition,
  },
  loading: {
    name: 'loading',
    description: 'Состояние загрузки',
    table: {
      category: 'Story',
      defaultValue: { summary: false },
      type: { summary: 'boolean' },
    },
    control: { type: 'boolean' },
    defaultValue: false,
  },
  size: {
    name: 'size',
    description: 'Размер',
    table: {
      category: 'Story',
      defaultValue: { summary: Button.defaultProps?.size },
      type: { summary: `${ButtonSize.Small} | ${ButtonSize.Medium} ${ButtonSize.Large}` },
    },
    control: { type: 'select' },
    options: [ButtonSize.Small, ButtonSize.Medium, ButtonSize.Large],
    defaultValue: Button.defaultProps?.size,
  },
  variant: {
    name: 'variant',
    description: 'Вид',
    table: {
      category: 'Story',
      defaultValue: { summary: Button.defaultProps?.variant },
      type: {
        summary: `${ButtonVariant.Secondary} | ${ButtonVariant.Primary} | ${ButtonVariant.Primary} | ${ButtonVariant.Danger} | ${ButtonVariant.Link}`,
      },
    },
    control: { type: 'select' },
    options: [ButtonVariant.Secondary, ButtonVariant.Primary, ButtonVariant.Danger, ButtonVariant.Link],
    defaultValue: Button.defaultProps?.variant,
  },
  sizeSmallBorderRadius: {
    name: 'borderRadius',
    description: 'Устанавливает радиус скругления уголков рамки',
    type: 'text',
    defaultValue: Theme.sizes?.[ButtonSize.Small]?.borderRadius,
    table: {
      category: 'Size',
      subcategory: 'Настройка стилей для size="small"',
      defaultValue: {
        summary: Theme.sizes?.[ButtonSize.Small]?.borderRadius,
      },
      type: {
        summary: 'border-radius',
      },
    },
  },
  sizeSmallBorderWidth: {
    name: 'borderWidth',
    description: 'Задает толщину границы одновременно на всех сторонах элемента или индивидуально для каждой стороны',
    type: 'text',
    defaultValue: Theme.sizes?.[ButtonSize.Small]?.borderWidth,
    table: {
      category: 'Size',
      subcategory: 'Настройка стилей для size="small"',
      defaultValue: {
        summary: Theme.sizes?.[ButtonSize.Small]?.borderWidth,
      },
      type: {
        summary: 'border-width',
      },
    },
  },
  sizeSmallFontSize: {
    name: 'fontSize',
    description: 'Определяет размер шрифта элемента',
    type: 'text',
    defaultValue: Theme.sizes?.[ButtonSize.Small]?.fontSize,
    table: {
      category: 'Size',
      subcategory: 'Настройка стилей для size="small"',
      defaultValue: {
        summary: Theme.sizes?.[ButtonSize.Small]?.fontSize,
      },
      type: {
        summary: 'font-size',
      },
    },
  },
  sizeSmallLineHeight: {
    name: 'lineHeight',
    description: 'Устанавливает интерлиньяж (межстрочный интервал) текста, отсчет ведется от базовой линии шрифта',
    type: 'text',
    defaultValue: Theme.sizes?.[ButtonSize.Small]?.lineHeight,
    table: {
      category: 'Size',
      subcategory: 'Настройка стилей для size="small"',
      defaultValue: {
        summary: Theme.sizes?.[ButtonSize.Small]?.lineHeight,
      },
      type: {
        summary: 'line-height',
      },
    },
  },
  sizeSmallPadding: {
    name: 'padding',
    description: 'Устанавливает значение полей вокруг содержимого элемента',
    type: 'text',
    defaultValue: Theme.sizes?.[ButtonSize.Small]?.padding,
    table: {
      category: 'Size',
      subcategory: 'Настройка стилей для size="small"',
      defaultValue: {
        summary: Theme.sizes?.[ButtonSize.Small]?.padding,
      },
      type: {
        summary: 'padding',
      },
    },
  },
  sizeMediumBorderRadius: {
    name: 'borderRadius',
    description: 'Устанавливает радиус скругления уголков рамки',
    type: 'text',
    defaultValue: Theme.sizes?.[ButtonSize.Medium]?.borderRadius,
    table: {
      category: 'Size',
      subcategory: 'Настройка стилей для size="medium"',
      defaultValue: {
        summary: Theme.sizes?.[ButtonSize.Medium]?.borderRadius,
      },
      type: {
        summary: 'border-radius',
      },
    },
  },
  sizeMediumBorderWidth: {
    name: 'borderWidth',
    description: 'Задает толщину границы одновременно на всех сторонах элемента или индивидуально для каждой стороны',
    type: 'text',
    defaultValue: Theme.sizes?.[ButtonSize.Medium]?.borderWidth,
    table: {
      category: 'Size',
      subcategory: 'Настройка стилей для size="medium"',
      defaultValue: {
        summary: Theme.sizes?.[ButtonSize.Medium]?.borderWidth,
      },
      type: {
        summary: 'border-width',
      },
    },
  },
  sizeMediumFontSize: {
    name: 'fontSize',
    description: 'Определяет размер шрифта элемента',
    type: 'text',
    defaultValue: Theme.sizes?.[ButtonSize.Medium]?.fontSize,
    table: {
      category: 'Size',
      subcategory: 'Настройка стилей для size="medium"',
      defaultValue: {
        summary: Theme.sizes?.[ButtonSize.Medium]?.fontSize,
      },
      type: {
        summary: 'font-size',
      },
    },
  },
  sizeMediumLineHeight: {
    name: 'lineHeight',
    description: 'Устанавливает интерлиньяж (межстрочный интервал) текста, отсчет ведется от базовой линии шрифта',
    type: 'text',
    defaultValue: Theme.sizes?.[ButtonSize.Medium]?.lineHeight,
    table: {
      category: 'Size',
      subcategory: 'Настройка стилей для size="medium"',
      defaultValue: {
        summary: Theme.sizes?.[ButtonSize.Medium]?.lineHeight,
      },
      type: {
        summary: 'line-height',
      },
    },
  },
  sizeMediumPadding: {
    name: 'padding',
    description: 'Устанавливает значение полей вокруг содержимого элемента',
    type: 'text',
    defaultValue: Theme.sizes?.[ButtonSize.Medium]?.padding,
    table: {
      category: 'Size',
      subcategory: 'Настройка стилей для size="medium"',
      defaultValue: {
        summary: Theme.sizes?.[ButtonSize.Medium]?.padding,
      },
      type: {
        summary: 'padding',
      },
    },
  },
  sizeLargeBorderRadius: {
    name: 'borderRadius',
    description: 'Устанавливает радиус скругления уголков рамки',
    type: 'text',
    defaultValue: Theme.sizes?.[ButtonSize.Large]?.borderRadius,
    table: {
      category: 'Size',
      subcategory: 'Настройка стилей для size="large"',
      defaultValue: {
        summary: Theme.sizes?.[ButtonSize.Large]?.borderRadius,
      },
      type: {
        summary: 'border-radius',
      },
    },
  },
  sizeLargeBorderWidth: {
    name: 'borderWidth',
    description: 'Задает толщину границы одновременно на всех сторонах элемента или индивидуально для каждой стороны',
    type: 'text',
    defaultValue: Theme.sizes?.[ButtonSize.Large]?.borderWidth,
    table: {
      category: 'Size',
      subcategory: 'Настройка стилей для size="large"',
      defaultValue: {
        summary: Theme.sizes?.[ButtonSize.Large]?.borderWidth,
      },
      type: {
        summary: 'border-width',
      },
    },
  },
  sizeLargeFontSize: {
    name: 'fontSize',
    description: 'Определяет размер шрифта элемента',
    type: 'text',
    defaultValue: Theme.sizes?.[ButtonSize.Large]?.fontSize,
    table: {
      category: 'Size',
      subcategory: 'Настройка стилей для size="large"',
      defaultValue: {
        summary: Theme.sizes?.[ButtonSize.Large]?.fontSize,
      },
      type: {
        summary: 'font-size',
      },
    },
  },
  sizeLargeLineHeight: {
    name: 'lineHeight',
    description: 'Устанавливает интерлиньяж (межстрочный интервал) текста, отсчет ведется от базовой линии шрифта',
    type: 'text',
    defaultValue: Theme.sizes?.[ButtonSize.Large]?.lineHeight,
    table: {
      category: 'Size',
      subcategory: 'Настройка стилей для size="large"',
      defaultValue: {
        summary: Theme.sizes?.[ButtonSize.Large]?.lineHeight,
      },
      type: {
        summary: 'line-height',
      },
    },
  },
  sizeLargePadding: {
    name: 'padding',
    description: 'Устанавливает значение полей вокруг содержимого элемента',
    type: 'text',
    defaultValue: Theme.sizes?.[ButtonSize.Large]?.padding,
    table: {
      category: 'Size',
      subcategory: 'Настройка стилей для size="large"',
      defaultValue: {
        summary: Theme.sizes?.[ButtonSize.Large]?.padding,
      },
      type: {
        summary: 'padding',
      },
    },
  },

  commonAlignItems: {
    name: 'alignItems',
    description: 'Align items',
    type: 'select',
    options: ['flex-start', 'flex-end', 'center', 'baseline', 'stretch'],
    defaultValue: Theme.common?.alignItems,
    table: {
      category: 'Common',
      defaultValue: { summary: Theme.common?.alignItems },
      type: { summary: `"flex-start" | "flex-end" | "center" | "baseline" | "stretch"` },
    },
  },
  commonBorderStyle: {
    name: 'borderStyle',
    description: 'Border style',
    type: 'select',
    options: 'none|hidden|dotted|dashed|solid|double|groove|ridge|inset|outset'.split('|'),
    defaultValue: Theme.common?.borderStyle,
    table: {
      category: 'Common',
      defaultValue: { summary: Theme.common?.borderStyle },
      type: { summary: 'none|hidden|dotted|dashed|solid|double|groove|ridge|inset|outset' },
    },
  },
  commonBoxSizing: {
    name: 'boxSizing',
    description: 'Box sizing',
    type: 'select',
    options: 'content-box|border-box'.split('|'),
    defaultValue: Theme.common?.boxSizing,
    table: {
      category: 'Common',
      defaultValue: { summary: Theme.common?.boxSizing },
      type: { summary: 'content-box | border-box' },
    },
  },
  commonCursor: {
    name: 'cursor',
    description: 'Cursor',
    type: 'select',
    options:
      `auto | default | none | context-menu | help | pointer | progress | wait | cell | crosshair | text | vertical-text | alias | copy | move | no-drop | not-allowed | grab | grabbing | e-resize | n-resize | ne-resize | nw-resize | s-resize | se-resize | sw-resize | w-resize | ew-resize | ns-resize | nesw-resize | nwse-resize | col-resize | row-resize | all-scroll | zoom-in | zoom-out`
        .replace(/\s/g, '')
        .split('|'),
    defaultValue: Theme.common?.cursor,
    table: {
      category: 'Common',
      defaultValue: { summary: Theme.common?.cursor },
      type: {
        summary:
          'auto | default | none | context-menu | help | pointer | progress | wait | cell | crosshair | text | vertical-text | alias | copy | move | no-drop | not-allowed | grab | grabbing | e-resize | n-resize | ne-resize | nw-resize | s-resize | se-resize | sw-resize | w-resize | ew-resize | ns-resize | nesw-resize | nwse-resize | col-resize | row-resize | all-scroll | zoom-in | zoom-out',
      },
    },
  },
  commonCursorDisabled: {
    name: 'cursorDisabled',
    description: 'Cursor when component disabled',
    type: 'select',
    options:
      `auto | default | none | context-menu | help | pointer | progress | wait | cell | crosshair | text | vertical-text | alias | copy | move | no-drop | not-allowed | grab | grabbing | e-resize | n-resize | ne-resize | nw-resize | s-resize | se-resize | sw-resize | w-resize | ew-resize | ns-resize | nesw-resize | nwse-resize | col-resize | row-resize | all-scroll | zoom-in | zoom-out`
        .replace(/\s/g, '')
        .split('|'),
    defaultValue: Theme.common?.cursorDisabled,
    table: {
      category: 'Common',
      defaultValue: { summary: Theme.common?.cursorDisabled },
      type: {
        summary:
          'auto | default | none | context-menu | help | pointer | progress | wait | cell | crosshair | text | vertical-text | alias | copy | move | no-drop | not-allowed | grab | grabbing | e-resize | n-resize | ne-resize | nw-resize | s-resize | se-resize | sw-resize | w-resize | ew-resize | ns-resize | nesw-resize | nwse-resize | col-resize | row-resize | all-scroll | zoom-in | zoom-out',
      },
    },
  },
  commonDisplay: {
    name: 'display',
    description: 'Display',
    type: 'select',
    options:
      `block | inline | inline-block | inline-table | inline-flex | flex | list-item | none |  run-in | table | table-caption | table-cell | table-column-group | table-column | table-footer-group | table-header-group | table-row | table-row-group`
        .replace(/\s/g, '')
        .split('|'),
    defaultValue: Theme.common?.display,
    table: {
      category: 'Common',
      defaultValue: { summary: Theme.common?.display },
      type: {
        summary: `block | inline | inline-block | inline-table | inline-flex | flex | list-item | none |  run-in | table | table-caption | table-cell | table-column-group | table-column | table-footer-group | table-header-group | table-row | table-row-group`,
      },
    },
  },
  commonJustifyContent: {
    name: 'justifyContent',
    description: 'Justify content',
    type: 'select',
    options: `flex-start | flex-end | center | space-between | space-around | space-evenly`
      .replace(/\s/g, '')
      .split('|'),
    defaultValue: Theme.common?.justifyContent,
    table: {
      category: 'Common',
      defaultValue: { summary: Theme.common?.justifyContent },
      type: { summary: `flex-start | flex-end | center | space-between | space-around | space-evenly` },
    },
  },
  commonOpacityDisabled: {
    name: 'opacityDisabled',
    description: 'Opacity when component disabled',
    type: 'number',
    control: {
      type: 'number',
      max: 1,
      min: 0,
      step: 0.05,
    },
    defaultValue: Theme.common?.opacityDisabled,
    table: {
      category: 'Common',
      defaultValue: { summary: Theme.common?.opacityDisabled },
      type: { summary: 'number' },
    },
  },
  commonOutlineOffset: {
    name: 'outlineOffset',
    description: 'Outline offset',
    type: 'number',
    control: {
      type: 'number',
      max: 20,
      min: -20,
      step: 1,
    },
    defaultValue: Theme.common?.outlineOffset,
    table: {
      category: 'Common',
      defaultValue: { summary: Theme.common?.outlineOffset },
      type: { summary: 'number' },
    },
  },
  commonTextDecoration: {
    name: 'textDecoration',
    description: 'Text decoration',
    type: 'select',
    options: `none | line-through | overline | underline | blink`.replace(/\s/g, '').split('|'),
    defaultValue: Theme.common?.textDecoration,
    table: {
      category: 'Common',
      defaultValue: { summary: Theme.common?.textDecoration },
      type: { summary: `none | line-through | overline | underline | blink` },
    },
  },
  commonTextTransform: {
    name: 'textTransform',
    description: 'Text transform',
    type: 'select',
    options: 'capitalize | lowercase | uppercase | none'.replace(/\s/g, '').split('|'),
    defaultValue: Theme.common?.textTransform,
    table: {
      category: 'Common',
      defaultValue: { summary: Theme.common?.textTransform },
      type: { summary: 'capitalize | lowercase | uppercase | none' },
    },
  },
  ...SecondaryStatesStyles,
  ...PrimaryStatesStyles,
  ...DangerStatesStyles,
  ...LinkStatesStyles,
};
