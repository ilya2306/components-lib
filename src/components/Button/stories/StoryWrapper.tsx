import styled from 'styled-components';

export const StyledStoryWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 16px;
`;

export const StyledButtonsLineWrapper = styled(StyledStoryWrapper)`
  column-gap: 16px;
`;
