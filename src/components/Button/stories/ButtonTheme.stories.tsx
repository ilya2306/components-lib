import React from 'react';
import { Button } from '../Button';
import { ButtonIconPositionType, ButtonSize, ButtonSizeType, ButtonVariant, ButtonVariantType } from '../@typings';

import { ThemeProvider } from 'styled-components';
import { createTheme } from '../../../theme/utils/create-theme';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function mapPropsToTheme(props: any) {
  return {
    Button: {
      sizes: {
        [ButtonSize.Small]: {
          borderRadius: props.sizeSmallBorderRadius,
          borderWidth: props.sizeSmallBorderWidth,
          fontSize: props.sizeSmallFontSize,
          lineHeight: props.sizeSmallLineHeight,
          padding: props.sizeSmallPadding,
        },
        [ButtonSize.Medium]: {
          borderRadius: props.sizeMediumBorderRadius,
          borderWidth: props.sizeMediumBorderWidth,
          fontSize: props.sizeMediumFontSize,
          lineHeight: props.sizeMediumLineHeight,
          padding: props.sizeMediumPadding,
        },
        [ButtonSize.Large]: {
          borderRadius: props.sizeLargeBorderRadius,
          borderWidth: props.sizeLargeBorderWidth,
          fontSize: props.sizeLargeFontSize,
          lineHeight: props.sizeLargeLineHeight,
          padding: props.sizeLargePadding,
        },
      },
      variants: {
        [ButtonVariant.Secondary]: {
          static: {
            background: props.variantSecondaryStaticBackground,
            borderColor: props.variantSecondaryStaticBorderColor,
            borderStyle: props.variantSecondaryStaticBorderStyle,
            boxShadow: props.variantSecondaryStaticBoxShadow,
            color: props.variantSecondaryStaticColor,
            opacity: props.variantSecondaryStaticOpacity,
            outline: props.variantSecondaryStaticOutline,
          },
          active: {
            background: props.variantSecondaryActiveBackground,
            borderColor: props.variantSecondaryActiveBorderColor,
            borderStyle: props.variantSecondaryActiveBorderStyle,
            boxShadow: props.variantSecondaryActiveBoxShadow,
            color: props.variantSecondaryActiveColor,
            opacity: props.variantSecondaryActiveOpacity,
            outline: props.variantSecondaryActiveOutline,
          },
          focus: {
            background: props.variantSecondaryFocusBackground,
            borderColor: props.variantSecondaryFocusBorderColor,
            borderStyle: props.variantSecondaryFocusBorderStyle,
            boxShadow: props.variantSecondaryFocusBoxShadow,
            color: props.variantSecondaryFocusColor,
            opacity: props.variantSecondaryFocusOpacity,
            outline: props.variantSecondaryFocusOutline,
          },
          focusVisible: {
            background: props.variantSecondaryFocusVisibleBackground,
            borderColor: props.variantSecondaryFocusVisibleBorderColor,
            borderStyle: props.variantSecondaryFocusVisibleBorderStyle,
            boxShadow: props.variantSecondaryFocusVisibleBoxShadow,
            color: props.variantSecondaryFocusVisibleColor,
            opacity: props.variantSecondaryFocusVisibleOpacity,
            outline: props.variantSecondaryFocusVisibleOutline,
          },
          hover: {
            background: props.variantSecondaryHoverBackground,
            borderColor: props.variantSecondaryHoverBorderColor,
            borderStyle: props.variantSecondaryHoverBorderStyle,
            boxShadow: props.variantSecondaryHoverBoxShadow,
            color: props.variantSecondaryHoverColor,
            opacity: props.variantSecondaryHoverOpacity,
            outline: props.variantSecondaryHoverOutline,
          },
          disabled: {
            background: props.variantSecondaryDisabledBackground,
            borderColor: props.variantSecondaryDisabledBorderColor,
            borderStyle: props.variantSecondaryDisabledBorderStyle,
            boxShadow: props.variantSecondaryDisabledBoxShadow,
            color: props.variantSecondaryDisabledColor,
            opacity: props.variantSecondaryDisabledOpacity,
            outline: props.variantSecondaryDisabledOutline,
          },
        },
        [ButtonVariant.Primary]: {
          static: {
            background: props.variantPrimaryStaticBackground,
            borderColor: props.variantPrimaryStaticBorderColor,
            borderStyle: props.variantPrimaryStaticBorderStyle,
            boxShadow: props.variantPrimaryStaticBoxShadow,
            color: props.variantPrimaryStaticColor,
            opacity: props.variantPrimaryStaticOpacity,
            outline: props.variantPrimaryStaticOutline,
          },
          active: {
            background: props.variantPrimaryActiveBackground,
            borderColor: props.variantPrimaryActiveBorderColor,
            borderStyle: props.variantPrimaryActiveBorderStyle,
            boxShadow: props.variantPrimaryActiveBoxShadow,
            color: props.variantPrimaryActiveColor,
            opacity: props.variantPrimaryActiveOpacity,
            outline: props.variantPrimaryActiveOutline,
          },
          focus: {
            background: props.variantPrimaryFocusBackground,
            borderColor: props.variantPrimaryFocusBorderColor,
            borderStyle: props.variantPrimaryFocusBorderStyle,
            boxShadow: props.variantPrimaryFocusBoxShadow,
            color: props.variantPrimaryFocusColor,
            opacity: props.variantPrimaryFocusOpacity,
            outline: props.variantPrimaryFocusOutline,
          },
          focusVisible: {
            background: props.variantPrimaryFocusVisibleBackground,
            borderColor: props.variantPrimaryFocusVisibleBorderColor,
            borderStyle: props.variantPrimaryFocusVisibleBorderStyle,
            boxShadow: props.variantPrimaryFocusVisibleBoxShadow,
            color: props.variantPrimaryFocusVisibleColor,
            opacity: props.variantPrimaryFocusVisibleOpacity,
            outline: props.variantPrimaryFocusVisibleOutline,
          },
          hover: {
            background: props.variantPrimaryHoverBackground,
            borderColor: props.variantPrimaryHoverBorderColor,
            borderStyle: props.variantPrimaryHoverBorderStyle,
            boxShadow: props.variantPrimaryHoverBoxShadow,
            color: props.variantPrimaryHoverColor,
            opacity: props.variantPrimaryHoverOpacity,
            outline: props.variantPrimaryHoverOutline,
          },
          disabled: {
            background: props.variantPrimaryDisabledBackground,
            borderColor: props.variantPrimaryDisabledBorderColor,
            borderStyle: props.variantPrimaryDisabledBorderStyle,
            boxShadow: props.variantPrimaryDisabledBoxShadow,
            color: props.variantPrimaryDisabledColor,
            opacity: props.variantPrimaryDisabledOpacity,
            outline: props.variantPrimaryDisabledOutline,
          },
        },
        [ButtonVariant.Link]: {
          static: {
            background: props.variantLinkStaticBackground,
            borderColor: props.variantLinkStaticBorderColor,
            borderStyle: props.variantLinkStaticBorderStyle,
            boxShadow: props.variantLinkStaticBoxShadow,
            color: props.variantLinkStaticColor,
            opacity: props.variantLinkStaticOpacity,
            outline: props.variantLinkStaticOutline,
          },
          active: {
            background: props.variantLinkActiveBackground,
            borderColor: props.variantLinkActiveBorderColor,
            borderStyle: props.variantLinkActiveBorderStyle,
            boxShadow: props.variantLinkActiveBoxShadow,
            color: props.variantLinkActiveColor,
            opacity: props.variantLinkActiveOpacity,
            outline: props.variantLinkActiveOutline,
          },
          focus: {
            background: props.variantLinkFocusBackground,
            borderColor: props.variantLinkFocusBorderColor,
            borderStyle: props.variantLinkFocusBorderStyle,
            boxShadow: props.variantLinkFocusBoxShadow,
            color: props.variantLinkFocusColor,
            opacity: props.variantLinkFocusOpacity,
            outline: props.variantLinkFocusOutline,
          },
          focusVisible: {
            background: props.variantLinkFocusVisibleBackground,
            borderColor: props.variantLinkFocusVisibleBorderColor,
            borderStyle: props.variantLinkFocusVisibleBorderStyle,
            boxShadow: props.variantLinkFocusVisibleBoxShadow,
            color: props.variantLinkFocusVisibleColor,
            opacity: props.variantLinkFocusVisibleOpacity,
            outline: props.variantLinkFocusVisibleOutline,
          },
          hover: {
            background: props.variantLinkHoverBackground,
            borderColor: props.variantLinkHoverBorderColor,
            borderStyle: props.variantLinkHoverBorderStyle,
            boxShadow: props.variantLinkHoverBoxShadow,
            color: props.variantLinkHoverColor,
            opacity: props.variantLinkHoverOpacity,
            outline: props.variantLinkHoverOutline,
          },
          disabled: {
            background: props.variantLinkDisabledBackground,
            borderColor: props.variantLinkDisabledBorderColor,
            borderStyle: props.variantLinkDisabledBorderStyle,
            boxShadow: props.variantLinkDisabledBoxShadow,
            color: props.variantLinkDisabledColor,
            opacity: props.variantLinkDisabledOpacity,
            outline: props.variantLinkDisabledOutline,
          },
        },
        [ButtonVariant.Link]: {
          static: {
            background: props.variantLinkStaticBackground,
            borderColor: props.variantLinkStaticBorderColor,
            borderStyle: props.variantLinkStaticBorderStyle,
            boxShadow: props.variantLinkStaticBoxShadow,
            color: props.variantLinkStaticColor,
            opacity: props.variantLinkStaticOpacity,
            outline: props.variantLinkStaticOutline,
          },
          active: {
            background: props.variantLinkActiveBackground,
            borderColor: props.variantLinkActiveBorderColor,
            borderStyle: props.variantLinkActiveBorderStyle,
            boxShadow: props.variantLinkActiveBoxShadow,
            color: props.variantLinkActiveColor,
            opacity: props.variantLinkActiveOpacity,
            outline: props.variantLinkActiveOutline,
          },
          focus: {
            background: props.variantLinkFocusBackground,
            borderColor: props.variantLinkFocusBorderColor,
            borderStyle: props.variantLinkFocusBorderStyle,
            boxShadow: props.variantLinkFocusBoxShadow,
            color: props.variantLinkFocusColor,
            opacity: props.variantLinkFocusOpacity,
            outline: props.variantLinkFocusOutline,
          },
          focusVisible: {
            background: props.variantLinkFocusVisibleBackground,
            borderColor: props.variantLinkFocusVisibleBorderColor,
            borderStyle: props.variantLinkFocusVisibleBorderStyle,
            boxShadow: props.variantLinkFocusVisibleBoxShadow,
            color: props.variantLinkFocusVisibleColor,
            opacity: props.variantLinkFocusVisibleOpacity,
            outline: props.variantLinkFocusVisibleOutline,
          },
          hover: {
            background: props.variantLinkHoverBackground,
            borderColor: props.variantLinkHoverBorderColor,
            borderStyle: props.variantLinkHoverBorderStyle,
            boxShadow: props.variantLinkHoverBoxShadow,
            color: props.variantLinkHoverColor,
            opacity: props.variantLinkHoverOpacity,
            outline: props.variantLinkHoverOutline,
          },
          disabled: {
            background: props.variantLinkDisabledBackground,
            borderColor: props.variantLinkDisabledBorderColor,
            borderStyle: props.variantLinkDisabledBorderStyle,
            boxShadow: props.variantLinkDisabledBoxShadow,
            color: props.variantLinkDisabledColor,
            opacity: props.variantLinkDisabledOpacity,
            outline: props.variantLinkDisabledOutline,
          },
        },
      },
      common: {
        alignItems: props.commonAlignItems,
        borderStyle: props.commonBorderStyle,
        boxSizing: props.commonBoxSizing,
        cursor: props.commonCursor,
        cursorDisabled: props.commonCursorDisabled,
        display: props.commonDisplay,
        justifyContent: props.commonJustifyContent,
        opacityDisabled: props.commonOpacityDisabled,
        outlineOffset: props.commonOutlineOffset,
        textDecoration: props.commonTextDecoration,
        textTransform: props.commonTextTransform,
      },
    },
  };
}

export const DeleteIcon = () => (
  <svg viewBox="0 0 24 24" aria-hidden="true" width={16} height={16}>
    <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z" />
  </svg>
);

export const SendIcon = () => (
  <svg viewBox="0 0 24 24" aria-hidden="true" width={16} height={16}>
    <path d="M2.01 21 23 12 2.01 3 2 10l15 2-15 2z"></path>
  </svg>
);

export const ICONS = {
  delete: DeleteIcon,
  send: SendIcon,
};

export const ButtonTheme = (props: ButtonThemeProps) => {
  const { disabled, icon: propsIcon, iconPosition, loading, size, variant } = props;
  const Icon = propsIcon && propsIcon in ICONS ? ICONS[propsIcon] : undefined;

  return (
    <ThemeProvider theme={createTheme(mapPropsToTheme(props))}>
      <Button
        variant={variant}
        size={size}
        loading={loading}
        disabled={disabled}
        icon={Icon ? <Icon /> : undefined}
        iconPosition={iconPosition}
      >
        {props.children}
      </Button>
    </ThemeProvider>
  );
};

type ButtonThemeProps = {
  children: React.ReactChildren;
  disabled: boolean;
  icon: React.ReactNode;
  iconPosition: ButtonIconPositionType;
  loading: boolean;
  size: ButtonSizeType;
  variant: ButtonVariantType;
};
