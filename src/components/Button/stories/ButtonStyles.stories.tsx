import React from 'react';
import { Button } from '../Button';
import { ButtonVariant } from '../@typings';

import { StyledStoryWrapper } from './StoryWrapper';

export const ButtonStyles = () => (
  <StyledStoryWrapper style={{ gap: 16 }}>
    <Button>Default</Button>
    <Button variant={ButtonVariant.Primary}>Primary</Button>
    <Button variant={ButtonVariant.Danger}>Danger</Button>
    <Button variant={ButtonVariant.Link}>Link</Button>
  </StyledStoryWrapper>
);

ButtonStyles.parameters = {
  docs: {
    source: {
      code: `import Button from 'moneta-ui/Button';

<Button>Default</Button>
<Button variant={Button.Variant.Primary}>Primary</Button>
<Button variant={Button.Variant.Danger}>Danger</Button>
<Button variant={Button.Variant.Link}>Link</Button>
      `,
    },
  },
};
