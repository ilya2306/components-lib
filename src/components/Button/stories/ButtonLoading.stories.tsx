import React from 'react';
import { Button } from '../Button';
import { ButtonVariant, ButtonIconPosition } from '../@typings';
import { StyledStoryWrapper } from './StoryWrapper';

const DeleteIcon = () => (
  <svg viewBox="0 0 24 24" aria-hidden="true" width={16} height={16}>
    <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z" />
  </svg>
);

const SendIcon = () => (
  <svg viewBox="0 0 24 24" aria-hidden="true" width={16} height={16}>
    <path d="M2.01 21 23 12 2.01 3 2 10l15 2-15 2z"></path>
  </svg>
);

export const ButtonLoading = () => (
  <StyledStoryWrapper style={{ gap: 16 }}>
    <Button loading>Default</Button>
    <Button variant={ButtonVariant.Primary} loading icon={<DeleteIcon />} iconPosition={ButtonIconPosition.Left}>
      Primary
    </Button>
    <Button variant={ButtonVariant.Danger} loading icon={<SendIcon />}>
      Danger
    </Button>
  </StyledStoryWrapper>
);

ButtonLoading.parameters = {
  docs: {
    source: {
      code: `import Button from 'moneta-ui/Button';

<Button size={Button.Size.Small}>Small</Button>
<Button size={Button.Size.Medium}>Medium</Button>
<Button size={Button.Size.Large}>Large</Button>

<Button variant={Button.Variant.Primary} size={Button.Size.Small}>Small</Button>
<Button variant={Button.Variant.Primary} size={Button.Size.Medium}>Medium</Button>
<Button variant={Button.Variant.Primary} size={Button.Size.Large}>Large</Button>

<Button variant={Button.Variant.Danger} size={Button.Size.Small}>Small</Button>
<Button variant={Button.Variant.Danger} size={Button.Size.Medium}>Medium</Button>
<Button variant={Button.Variant.Danger} size={Button.Size.Large}>Large</Button>

<Button variant={Button.Variant.Link} size={Button.Size.Small}>Small</Button>
<Button variant={Button.Variant.Link} size={Button.Size.Medium}>Medium</Button>
<Button variant={Button.Variant.Link} size={Button.Size.Large}>Large</Button>`,
    },
  },
};
