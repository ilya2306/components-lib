import React from 'react';
import { Button } from '../Button';
import { ButtonVariant, ButtonSize } from '../@typings';
import { StyledButtonsLineWrapper, StyledStoryWrapper } from './StoryWrapper';

export const ButtonSizes = () => (
  <StyledStoryWrapper style={{ flexDirection: 'column', gap: 16 }}>
    <StyledButtonsLineWrapper>
      <Button size={ButtonSize.Small}>Small</Button>
      <Button size={ButtonSize.Medium}>Medium</Button>
      <Button size={ButtonSize.Large}>Large</Button>
    </StyledButtonsLineWrapper>
    <StyledButtonsLineWrapper>
      <Button variant={ButtonVariant.Primary} size={ButtonSize.Small}>
        Small
      </Button>
      <Button variant={ButtonVariant.Primary} size={ButtonSize.Medium}>
        Medium
      </Button>
      <Button variant={ButtonVariant.Primary} size={ButtonSize.Large}>
        Large
      </Button>
    </StyledButtonsLineWrapper>
    <StyledButtonsLineWrapper>
      <Button variant={ButtonVariant.Danger} size={ButtonSize.Small}>
        Small
      </Button>
      <Button variant={ButtonVariant.Danger} size={ButtonSize.Medium}>
        Medium
      </Button>
      <Button variant={ButtonVariant.Danger} size={ButtonSize.Large}>
        Large
      </Button>
    </StyledButtonsLineWrapper>
    <StyledButtonsLineWrapper>
      <Button variant={ButtonVariant.Link} size={ButtonSize.Small}>
        Small
      </Button>
      <Button variant={ButtonVariant.Link} size={ButtonSize.Medium}>
        Medium
      </Button>
      <Button variant={ButtonVariant.Link} size={ButtonSize.Large}>
        Large
      </Button>
    </StyledButtonsLineWrapper>
  </StyledStoryWrapper>
);

ButtonSizes.parameters = {
  docs: {
    source: {
      code: `import Button from 'moneta-ui/Button';

<Button size={Button.Size.Small}>Small</Button>
<Button size={Button.Size.Medium}>Medium</Button>
<Button size={Button.Size.Large}>Large</Button>

<Button variant={Button.Variant.Primary} size={Button.Size.Small}>Small</Button>
<Button variant={Button.Variant.Primary} size={Button.Size.Medium}>Medium</Button>
<Button variant={Button.Variant.Primary} size={Button.Size.Large}>Large</Button>

<Button variant={Button.Variant.Danger} size={Button.Size.Small}>Small</Button>
<Button variant={Button.Variant.Danger} size={Button.Size.Medium}>Medium</Button>
<Button variant={Button.Variant.Danger} size={Button.Size.Large}>Large</Button>

<Button variant={Button.Variant.Link} size={Button.Size.Small}>Small</Button>
<Button variant={Button.Variant.Link} size={Button.Size.Medium}>Medium</Button>
<Button variant={Button.Variant.Link} size={Button.Size.Large}>Large</Button>`,
    },
  },
};
