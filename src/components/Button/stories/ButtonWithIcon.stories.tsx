import React from 'react';
import { Button } from '../Button';
import { ButtonIconPosition, ButtonVariant } from '../@typings';
import { StyledStoryWrapper } from './StoryWrapper';

const DeleteIcon = () => (
  <svg viewBox="0 0 24 24" aria-hidden="true" width={16} height={16}>
    <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z" />
  </svg>
);

const SendIcon = () => (
  <svg viewBox="0 0 24 24" aria-hidden="true" width={16} height={16}>
    <path d="M2.01 21 23 12 2.01 3 2 10l15 2-15 2z"></path>
  </svg>
);

export const ButtonWithIcon = () => (
  <StyledStoryWrapper>
    <Button variant={ButtonVariant.Primary} icon={<SendIcon />}>
      Send
    </Button>
    <Button icon={<DeleteIcon />} iconPosition={ButtonIconPosition.Left}>
      Delete
    </Button>
  </StyledStoryWrapper>
);

ButtonWithIcon.parameters = {
  docs: {
    source: {
      code: `import Button from "moneta-ui/Button"

const SendIcon = () => (
  <svg viewBox="0 0 24 24" aria-hidden="true" data-testid="SendIcon">
    <path d="M2.01 21 23 12 2.01 3 2 10l15 2-15 2z"></path>
  </svg>
);

<Button variant={Button.Variant.Primary} icon={<SendIcon />}>
  Send
</Button>

const DeleteIcon = () => (
  <svg viewBox="0 0 24 24" aria-hidden="true">
    <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z" />
  </svg>
);

<Button icon={<DeleteIcon />} iconPosition={Button.IconPosition.Left}>
  Delete
</Button>
`,
    },
  },
};
