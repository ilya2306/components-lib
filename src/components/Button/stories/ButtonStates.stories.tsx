import React from 'react';
import { Button } from '../Button';
import { ButtonVariant } from '../@typings';
import { StyledButtonsLineWrapper, StyledStoryWrapper } from './StoryWrapper';

export const ButtonStates = () => (
  <StyledStoryWrapper style={{ flexDirection: 'column', gap: 16 }}>
    <StyledButtonsLineWrapper>
      <Button>Default</Button>
      <Button disabled>Disabled</Button>
      <Button loading>Loading</Button>
    </StyledButtonsLineWrapper>
    <StyledButtonsLineWrapper>
      <Button variant={ButtonVariant.Primary}>Default</Button>
      <Button variant={ButtonVariant.Primary} disabled>
        Disabled
      </Button>
      <Button variant={ButtonVariant.Primary} loading>
        Loading
      </Button>
    </StyledButtonsLineWrapper>
    <StyledButtonsLineWrapper>
      <Button variant={ButtonVariant.Danger}>Default</Button>
      <Button variant={ButtonVariant.Danger} disabled>
        Disabled
      </Button>
      <Button variant={ButtonVariant.Danger} loading>
        Loading
      </Button>
    </StyledButtonsLineWrapper>
    <StyledButtonsLineWrapper>
      <Button variant={ButtonVariant.Link}>Default</Button>
      <Button variant={ButtonVariant.Link} disabled>
        Disabled
      </Button>
      <Button variant={ButtonVariant.Link} loading>
        Loading
      </Button>
    </StyledButtonsLineWrapper>
  </StyledStoryWrapper>
);

ButtonStates.parameters = {
  docs: {
    source: {
      code: `
import Button from 'moneta-ui/Button';

<Button>Default</Button>
<Button disabled>Disabled</Button>
<Button loading>Loading</Button>

<Button variant={Button.Variant.Primary}>Default</Button>
<Button variant={Button.Variant.Primary} disabled>Disabled</Button>
<Button variant={Button.Variant.Primary} loading>Loading</Button>

<Button variant={Button.Variant.Danger}>Default</Button>
<Button variant={Button.Variant.Danger} disabled>Disabled</Button>
<Button variant={Button.Variant.Danger} loading>Loading</Button>

<Button variant={Button.Variant.Link}>Default</Button>
<Button variant={Button.Variant.Link} disabled>Disabled</Button>
<Button variant={Button.Variant.Link} loading>Loading</Button>`,
    },
  },
};
