import { ButtonThemeType } from './@typings';
import { ButtonSize, ButtonVariant } from '../@typings';
import { hexToRGBA } from '../../../theme/Colors/utils';

export const Button: ButtonThemeType = {
  common: {
    alignItems: 'center',
    cursor: 'pointer',
    cursorDisabled: 'default',
    display: 'inline-flex',
    focusVisible: {
      boxShadow: '#111111 0px 0px 0px 1px inset',
      borderColor: '#111111',
    },
    textDecoration: 'none',
    outlineOffset: 0,
    borderStyle: 'solid',
  },
  variants: {
    [ButtonVariant.Secondary]: {
      static: {
        background: 'linear-gradient(180deg, #FFFFFF 0%, #F4F5F7 100%)',
        borderColor: '#C7D0D9',
        color: '#111111',
      },
      hover: {
        borderColor: '#A0AFBD',
      },
      active: {
        background: '#EEF0F3',
        borderColor: '#C7D0D9',
      },
      focusVisible: {
        borderColor: '#111111',
      },
      disabled: {
        background: `linear-gradient(180deg, ${hexToRGBA('#FFFFFF', 40)} 0%, ${hexToRGBA('#F4F5F7', 40)} 100%)`,
        color: hexToRGBA('#111111', 40),
        borderColor: hexToRGBA('#C7D0D9', 40),
      },
    },
    [ButtonVariant.Primary]: {
      static: {
        background: '#FFE083',
        borderColor: 'transparent',
        color: '#111111',
      },
      hover: {
        background: '#FFD350',
      },
      active: {
        background: '#FFCD38',
      },
      focus: {
        outline: 'none',
      },
      disabled: {
        background: hexToRGBA('#FFE083', 40),
        color: hexToRGBA('#111111', 40),
      },
    },
    [ButtonVariant.Danger]: {
      static: {
        color: '#FFFFFF',
        background: '#F03E3E',
        borderColor: 'transparent',
      },
      hover: {
        background: '#E91212',
      },
      active: {
        background: '#D11010',
      },
      disabled: {
        background: hexToRGBA('#F03E3E', 40),
      },
    },
    [ButtonVariant.Link]: {
      static: {
        color: '#1C7ED6',
        background: 'transparent',
        borderColor: 'transparent',
      },
      hover: {
        color: '#F03E3E',
      },
      active: {
        color: '#F03E3E',
        background: 'transparent',
      },
      disabled: {
        color: hexToRGBA('#1C7ED6', 40),
      },
    },
  },
  sizes: {
    [ButtonSize.Small]: {
      borderRadius: '4px',
      borderWidth: '1px',
      fontSize: '13px',
      lineHeight: '18px',
      padding: '9px 24px',
    },
    [ButtonSize.Medium]: {
      borderRadius: '4px',
      borderWidth: '1px',
      fontSize: '16px',
      lineHeight: '24px',
      padding: '8px 32px',
    },
    [ButtonSize.Large]: {
      borderRadius: '4px',
      borderWidth: '1px',
      fontSize: '16px',
      lineHeight: '24px',
      padding: '12px 32px',
    },
  },
};
