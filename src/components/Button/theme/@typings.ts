import { ButtonVariant, ButtonSize } from '../@typings';
import {
  CSSAlignItemsType,
  CSSBackgroundType,
  CSSBorderColorType,
  CSSBorderRadiusType,
  CSSBorderStyleType,
  CSSBorderWidthType,
  CSSBoxShadowType,
  CSSBoxSizingType,
  CSSColorType,
  CSSCursorType,
  CSSDisplayType,
  CSSFontSizeType,
  CSSJustifyContentType,
  CSSLineHeightType,
  CSSOpacityType,
  CSSOutlineOffsetType,
  CSSOutlineType,
  CSSPaddingType,
  CSSTextDecorationType,
  CSSTextTransformType,
} from '../../../theme/@typings/common';

export type ButtonThemeType = {
  common?: ButtonCommonType;
  variants?: {
    [ButtonVariant.Danger]?: ButtonVariantsRules;
    [ButtonVariant.Primary]?: ButtonVariantsRules;
    [ButtonVariant.Secondary]?: ButtonVariantsRules;
    [ButtonVariant.Link]?: ButtonVariantsRules;
  };
  sizes?: {
    [ButtonSize.Large]?: ButtonSizeRules;
    [ButtonSize.Medium]?: ButtonSizeRules;
    [ButtonSize.Small]?: ButtonSizeRules;
  };
  loader?: React.ReactNode;
};

export type ButtonCommonType = {
  alignItems?: CSSAlignItemsType;
  borderStyle?: CSSBorderStyleType;
  boxSizing?: CSSBoxSizingType;
  cursor?: CSSCursorType;
  cursorDisabled?: CSSCursorType;
  display?: CSSDisplayType;
  justifyContent?: CSSJustifyContentType;
  opacityDisabled?: CSSOpacityType;
  outlineOffset?: CSSOutlineOffsetType;
  textDecoration?: CSSTextDecorationType;
  textTransform?: CSSTextTransformType;
} & ButtonVariantsRules;

export type ButtonVariantsRules = {
  static?: ButtonVariantRules;
  hover?: ButtonVariantRules;
  active?: ButtonVariantRules;
  focus?: ButtonVariantRules;
  focusVisible?: ButtonVariantRules;
  disabled?: ButtonVariantRules;
};

export type ButtonVariantRules = {
  background?: CSSBackgroundType;
  borderColor?: CSSBorderColorType;
  borderStyle?: CSSBorderStyleType;
  boxShadow?: CSSBoxShadowType;
  color?: CSSColorType;
  opacity?: CSSOpacityType;
  outline?: CSSOutlineType;
};

export type ButtonSizeRules = {
  borderRadius?: CSSBorderRadiusType;
  borderWidth?: CSSBorderWidthType;
  fontSize?: CSSFontSizeType;
  lineHeight?: CSSLineHeightType;
  padding?: CSSPaddingType;
};
