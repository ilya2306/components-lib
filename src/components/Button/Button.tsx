import React from 'react';

import { ButtonProps, ButtonVariant, ButtonIconPosition } from './@typings';

import { SizeDefault, VariantDefault } from './config';

import { keyListener } from '../../lib/events/keyListener';

// import { Loader } from 'components/Loader';
import { Spinner } from '../Spinner/Spinner';

import { StyledButton } from './style/StyledButton';
import { StyledButtonIcon } from './style/StyledButtonIcon';
import { StyledButtonContent } from './style/StyledButtonContent';
import { StyledButtonLoader } from './style/StyledButtonLoader';
import { ThemeContext } from 'styled-components';
import { Loader } from '../Loader';

export const Button = React.forwardRef<HTMLButtonElement, ButtonProps>((props, ref) => {
  const {
    children: propsChildren,
    disabled: propsDisabled,
    href: propsHref,
    icon: propsIcon,
    iconPosition: propsIconPosition,
    loading: propsLoading,
    type: propsType,
    ...innerProps
  } = props;

  const themeContext = React.useContext(ThemeContext);

  const iconProps = {
    children: propsLoading ? <Spinner /> : propsIcon,
    disabled: propsDisabled,
    position: propsIconPosition,
    size: props.size,
    submitting: propsLoading,
    variant: props.variant,
  };

  let calculatedProps;

  const isLink = props.variant === ButtonVariant.Link;
  const hasIcon = !!propsIcon;

  if (isLink) {
    calculatedProps = {
      'aria-disabled': propsDisabled,
      href: propsHref,
    };
  } else {
    calculatedProps = {
      type: propsType,
    };
  }

  const rootRef = React.useRef<HTMLButtonElement>();

  function createRef(node: HTMLButtonElement) {
    rootRef.current = node;
    if (typeof ref === 'function') {
      ref(node);
    } else if (ref) {
      ref.current = node;
    }
  }

  React.useEffect(() => {
    if (props.autoFocus && !props.disabled) {
      keyListener.isTabPressed = true;
      rootRef.current?.focus();
    }
  }, []);

  const [focusedByTab, setFocusedByTab] = React.useState(false);

  const handleFocus = (e: React.FocusEvent<HTMLButtonElement>) => {
    if (!props.disabled && !props.loading) {
      requestAnimationFrame(() => {
        if (keyListener.isTabPressed) {
          setFocusedByTab(true);
        }
      });
      props.onFocus?.(e);
    }
  };

  const handleBlur = (e: React.FocusEvent<HTMLButtonElement>) => {
    setFocusedByTab(false);
    if (!props.disabled && !props.loading) {
      props.onBlur?.(e);
    }
  };

  const handleClick = (e: React.MouseEvent<HTMLButtonElement>) => {
    setFocusedByTab(false);
    if (!props.disabled && !props.loading) {
      props.onClick?.(e);
    }
  };

  const contentHidden = propsLoading && !propsIcon && !isLink;

  const RenderedLoader = themeContext?.Button?.loader || Loader;

  return (
    <StyledButton
      {...innerProps}
      {...calculatedProps}
      as={isLink ? 'a' : 'button'}
      disabled={propsDisabled || propsLoading}
      focusedByTab={focusedByTab}
      onBlur={handleBlur}
      onFocus={handleFocus}
      onClick={handleClick}
      ref={createRef}
      hasIcon={hasIcon}
      iconPosition={propsIconPosition}
      tabIndex={propsDisabled || propsLoading ? -1 : innerProps.tabIndex || 0}
    >
      <StyledButtonContent hidden={contentHidden}>{propsChildren}</StyledButtonContent>

      {hasIcon && <StyledButtonIcon {...iconProps} />}

      {propsLoading && !propsIcon && !isLink && (
        <StyledButtonLoader>
          <RenderedLoader monochrome={props.variant !== ButtonVariant.Secondary} size={props.size} />
        </StyledButtonLoader>
      )}

      {propsLoading && isLink && (
        <StyledButtonIcon
          {...iconProps}
          style={{
            position: 'absolute',
            right: '8px',
          }}
        >
          <Spinner />
        </StyledButtonIcon>
      )}
    </StyledButton>
  );
});

Button.defaultProps = {
  iconPosition: ButtonIconPosition.Right,
  size: SizeDefault,
  type: 'button',
  variant: VariantDefault,
};
