import { ButtonSize, ButtonVariant } from './@typings';

export const VariantDefault = ButtonVariant.Secondary;
export const SizeDefault = ButtonSize.Medium;
