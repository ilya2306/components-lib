import styled, { css } from 'styled-components';

export const StyledButtonContent = styled.span.withConfig({
  shouldForwardProp: (prop) => !['hidden'].includes(prop),
})`
  ${(props) =>
    props.hidden
      ? css`
          opacity: 0;
          visibility: hidden;
        `
      : css``}
`;
