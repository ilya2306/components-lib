import {
  ButtonCommonType,
  ButtonSizeRules,
  ButtonThemeType,
  ButtonVariantRules,
  ButtonVariantsRules,
} from '../theme/@typings';
import { ButtonSizeType, ButtonVariantType } from '../@typings';
import { SizeDefault } from '../config';
import { deepMerge } from '../../../theme/utils/utils';

export type CssCommonPropertiesNames = keyof ButtonCommonType;

export function resolveCssCommonValue<ValueType>(
  propertyName: CssCommonPropertiesNames,
  values: ButtonCommonType,
  defaultValue: ValueType | undefined = undefined,
): ValueType | undefined {
  let value: ValueType | undefined = defaultValue;

  if (propertyName && values && propertyName in values) {
    value = values[propertyName] as ValueType | undefined;
  }

  return value || defaultValue;
}

export type CssSizePropertiesNames = keyof ButtonSizeRules;

export function resolveCssSizeValue<ValueType>(
  propertyName: CssSizePropertiesNames,
  values: ButtonSizeRules,
  defaultValue: ValueType | undefined = undefined,
): ValueType | undefined {
  let value: ValueType | undefined = defaultValue;

  if (propertyName && values && propertyName in values) {
    value = values[propertyName] as ValueType | undefined;
  }

  return value || defaultValue;
}

export type CssVariantPropertyNameType = keyof ButtonVariantRules;

export function resolveCssVariantValue<ValueType>(
  propertyName: CssVariantPropertyNameType,
  variant: ButtonVariantRules,
  defaultValue: ValueType | undefined = undefined,
): ValueType | undefined {
  let value: ValueType | undefined = defaultValue;

  if (propertyName && variant) {
    if (propertyName in variant) {
      value = variant[propertyName] as ValueType | undefined;
    }
  }

  return value || defaultValue;
}

export function getMergedThemeSize(theme: ButtonThemeType, size: ButtonSizeType): Required<ButtonSizeRules> {
  const getMergedSizeRules = (ruleName: keyof ButtonSizeRules) =>
    deepMerge(theme.sizes?.[SizeDefault] || {}, theme.sizes?.[size] || {})[ruleName] || 'initial';

  return {
    borderRadius: getMergedSizeRules('borderRadius'),
    borderWidth: getMergedSizeRules('borderWidth'),
    fontSize: getMergedSizeRules('fontSize'),
    lineHeight: getMergedSizeRules('lineHeight'),
    padding: getMergedSizeRules('padding'),
  };
}

export function getMergedThemeVariants(
  theme: ButtonThemeType,
  variant: ButtonVariantType,
): Required<ButtonVariantsRules> {
  const getMergedVariantType = (type: keyof ButtonVariantsRules) =>
    deepMerge(
      theme.common?.static || {},
      theme.common?.[type] || {},
      theme.variants?.[variant]?.static || {},
      theme.variants?.[variant]?.[type] || {},
    );

  return {
    active: getMergedVariantType('active'),
    disabled: getMergedVariantType('disabled'),
    focus: getMergedVariantType('focus'),
    focusVisible: getMergedVariantType('focusVisible'),
    hover: getMergedVariantType('hover'),
    static: getMergedVariantType('static'),
  };
}
