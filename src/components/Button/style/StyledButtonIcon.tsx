import { ButtonIconPosition, ButtonIconPositionType, ButtonSizeType, ButtonVariantType } from '../@typings';

import styled from 'styled-components';

interface ButtonIconProps {
  disabled?: boolean;
  position: ButtonIconPositionType;
  size: ButtonSizeType;
  submitting?: boolean;
  variant?: ButtonVariantType;
}

export const StyledButtonIcon = styled.span.attrs((props: ButtonIconProps) => {
  const { disabled, position, size, submitting, variant } = props;

  const positionRight = position === ButtonIconPosition.Right;

  return {
    marginRight: `${positionRight ? 0 : 8}px`,
    marginLeft: `${positionRight ? 8 : 0}px`,
    height: '18px',
    width: '18px',
  };
})`
  display: inherit;
  margin-right: ${(props) => props.marginRight};
  margin-left: ${(props) => props.marginLeft};
  width: ${(props) => props.width};
  height: ${(props) => props.height};

  & svg {
    fill: currentColor;
  }
`;
