import styled from 'styled-components';

export const StyledButtonLoader = styled.span`
  align-items: center;
  display: flex;
  justify-content: center;
  left: 0;
  top: 0;
  position: absolute;
  width: 100%;
  height: 100%;
`;
