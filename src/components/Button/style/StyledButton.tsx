import styled, { css } from 'styled-components';

import {
  CSSAlignItemsType,
  CSSBackgroundType,
  CSSBorderColorType,
  CSSBorderRadiusType,
  CSSBorderStyleType,
  CSSBorderWidthType,
  CSSBoxShadowType,
  CSSBoxSizingType,
  CSSColorType,
  CSSCursorType,
  CSSDisplayType,
  CSSFontSizeType,
  CSSJustifyContentType,
  CSSLineHeightType,
  CSSOpacityType,
  CSSOutlineOffsetType,
  CSSPaddingType,
  CSSTextDecorationType,
  CSSTextTransformType,
} from '../../../theme/@typings/common';
import { ButtonIconPosition, ButtonProps, ButtonSizeType, ButtonVariant, ButtonVariantType } from '../@typings';
import { ThemeType } from '../../../theme/@typings/theme';

import {
  CssCommonPropertiesNames,
  CssSizePropertiesNames,
  CssVariantPropertyNameType,
  getMergedThemeSize,
  getMergedThemeVariants,
  resolveCssCommonValue,
  resolveCssSizeValue,
  resolveCssVariantValue,
} from './utils';
import {
  ButtonCommonType,
  ButtonSizeRules,
  ButtonThemeType,
  ButtonVariantRules,
  ButtonVariantsRules,
} from '../theme/@typings';

interface StyledButtonProps extends ButtonProps {
  focusedByTab: boolean;
  hasIcon: boolean;
  theme: ThemeType;
}

interface StyledButtonAttrs extends Omit<StyledButtonProps, 'theme|size|variant'> {
  commonStyles: ButtonCommonType;
  componentTheme: ButtonThemeType;
  defaultVariantState: ButtonVariantsRules;
  sizeState: Required<ButtonSizeRules>;
  variantState: Required<ButtonVariantsRules>;
}

export const StyledButton = styled.button.attrs((props: StyledButtonProps) => {
  const { theme, size, variant, ...innerProps } = props;

  const componentTheme = theme['Button'] as ButtonThemeType;
  const commonStyles = componentTheme?.common;

  return {
    ...innerProps,
    commonStyles,
    componentTheme,
    defaultVariantState: componentTheme.variants?.[ButtonVariant.Secondary],
    sizeState: getMergedThemeSize(componentTheme, size as ButtonSizeType),
    variantState: getMergedThemeVariants(componentTheme, variant as ButtonVariantType),
  };
})`
  ${(props: StyledButtonAttrs) => {
    const { variantState } = props;

    const resolveCommonValue = <ValueType,>(propertyName: CssCommonPropertiesNames, defaultValue?: ValueType) =>
      resolveCssCommonValue<ValueType>(propertyName, props.commonStyles, defaultValue);

    const resolveSizeValue = <ValueType,>(propertyName: CssSizePropertiesNames, defaultValue?: ValueType) =>
      resolveCssSizeValue<ValueType>(propertyName, props.sizeState, defaultValue);

    const makeCssVariantResolver =
      (rules: ButtonVariantRules) =>
      <ValueType,>(propertyName: CssVariantPropertyNameType, defaultValue?: ValueType) =>
        resolveCssVariantValue<ValueType>(propertyName, rules, defaultValue);

    const resolveCssVariantActiveValue = makeCssVariantResolver(variantState.active);
    const resolveCssVariantDisabledValue = makeCssVariantResolver(variantState.disabled);
    const resolveCssVariantFocusValue = makeCssVariantResolver(variantState.focus);
    const resolveCssVariantFocusVisibleValue = makeCssVariantResolver(variantState.focusVisible);
    const resolveCssVariantHoverValue = makeCssVariantResolver(variantState.hover);
    const resolveCssVariantStaticValue = makeCssVariantResolver(variantState.static);

    return css`
      position: relative;
      align-items: ${() => resolveCommonValue<CSSAlignItemsType>('alignItems')};
      border-radius: ${() => resolveSizeValue<CSSBorderRadiusType>('borderRadius')};
      border-width: ${() => resolveSizeValue<CSSBorderWidthType>('borderWidth')};
      box-sizing: ${() => resolveCommonValue<CSSBoxSizingType>('boxSizing')};
      display: ${() => resolveCommonValue<CSSDisplayType>('display')};
      font-size: ${() => resolveSizeValue<CSSFontSizeType>('fontSize')};
      justify-content: ${() => resolveCommonValue<CSSJustifyContentType>('justifyContent')};
      line-height: ${() => resolveSizeValue<CSSLineHeightType>('lineHeight')};
      padding: ${() => resolveSizeValue<CSSPaddingType>('padding')};
      text-transform: ${() => resolveCommonValue<CSSTextTransformType>('textTransform')};
      user-select: none;
      outline-offset: ${() => resolveCommonValue<CSSOutlineOffsetType>('outlineOffset')};
      text-decoration: ${() => resolveCommonValue<CSSTextDecorationType>('textDecoration')};
      border-style: ${() => resolveCommonValue<CSSBorderStyleType>('borderStyle')};
      outline: none;
      flex-direction: ${(props) =>
        props.hasIcon && props.iconPosition
          ? props.iconPosition === ButtonIconPosition.Right
            ? 'row'
            : 'row-reverse'
          : 'initial'};
      ${(props: StyledButtonAttrs) =>
        props.disabled
          ? css`
              background: ${() => resolveCssVariantDisabledValue<CSSBackgroundType>('background')};
              border-color: ${() => resolveCssVariantDisabledValue<CSSBorderColorType>('borderColor')};
              border-style: ${() => resolveCssVariantDisabledValue<CSSBorderStyleType>('borderStyle')};
              box-shadow: ${() => resolveCssVariantDisabledValue<CSSBoxShadowType>('boxShadow')};
              color: ${() => resolveCssVariantDisabledValue<CSSColorType>('color')};
              cursor: ${() => resolveCommonValue<CSSCursorType>('cursorDisabled')};
              opacity: ${() => resolveCssVariantDisabledValue<CSSOpacityType>('opacity')};
              pointer-events: none;
            `
          : props.focusedByTab
          ? css`
              background: ${() => resolveCssVariantFocusVisibleValue<CSSBackgroundType>('background')};
              border-color: ${() => resolveCssVariantFocusVisibleValue<CSSBorderColorType>('borderColor')};
              border-style: ${() => resolveCssVariantFocusVisibleValue<CSSBorderStyleType>('borderStyle')};
              box-shadow: ${() => resolveCssVariantFocusVisibleValue<CSSBoxShadowType>('boxShadow')};
              color: ${() => resolveCssVariantFocusVisibleValue<CSSColorType>('color')};
              opacity: ${() => resolveCssVariantFocusVisibleValue<CSSColorType>('opacity')};
            `
          : css`
              background: ${() => resolveCssVariantStaticValue<CSSBackgroundType>('background')};
              border-color: ${() => resolveCssVariantStaticValue<CSSBorderColorType>('borderColor')};
              border-style: ${() => resolveCssVariantStaticValue<CSSBorderStyleType>('borderStyle')};
              box-shadow: ${() => resolveCssVariantStaticValue<CSSBoxShadowType>('boxShadow')};
              color: ${() => resolveCssVariantStaticValue<CSSColorType>('color')};
              cursor: ${() => resolveCommonValue<CSSCursorType>('cursor', 'pointer')};
              opacity: ${() => resolveCssVariantStaticValue<CSSOpacityType>('opacity')};

              &:focus {
                background: ${() => resolveCssVariantFocusValue<CSSBackgroundType>('background')};
                border-color: ${() => resolveCssVariantFocusValue<CSSBorderColorType>('borderColor')};
                border-style: ${() => resolveCssVariantFocusValue<CSSBorderStyleType>('borderStyle')};
                box-shadow: ${() => resolveCssVariantFocusValue<CSSBoxShadowType>('boxShadow')};
                color: ${() => resolveCssVariantFocusValue<CSSColorType>('color')};
                opacity: ${() => resolveCssVariantFocusValue<CSSColorType>('opacity')};
              }

              &:hover {
                background: ${() => resolveCssVariantHoverValue<CSSBackgroundType>('background')};
                border-color: ${() => resolveCssVariantHoverValue<CSSBorderColorType>('borderColor')};
                border-style: ${() => resolveCssVariantHoverValue<CSSBorderStyleType>('borderStyle')};
                box-shadow: ${() => resolveCssVariantHoverValue<CSSBoxShadowType>('boxShadow')};
                color: ${() => resolveCssVariantHoverValue<CSSColorType>('color')};
              }

              &:active {
                background: ${() => resolveCssVariantActiveValue<CSSBackgroundType>('background')};
                border-color: ${() => resolveCssVariantActiveValue<CSSBorderColorType>('borderColor')};
                border-style: ${() => resolveCssVariantActiveValue<CSSBorderStyleType>('borderStyle')};
                box-shadow: ${() => resolveCssVariantActiveValue<CSSBoxShadowType>('boxShadow')};
                color: ${() => resolveCssVariantActiveValue<CSSColorType>('color')};
                opacity: ${() => resolveCssVariantActiveValue<CSSColorType>('opacity')};
              }
            `}
    `;
  }}
`;
