import { Button } from './Button';
import { ButtonSize as Size, ButtonVariant as Variant, ButtonIconPosition as IconPosition } from './@typings';

export { Button as theme } from './theme';

export default {
  Button,
  IconPosition,
  Size,
  Variant,
};

export { Button };

